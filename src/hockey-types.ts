import type { TeamAbbreviation } from "./teams.js";

export interface LocalizedName {
	default: string;
	fr?: string;
}

export interface LeagueScheduleWeek {
	/**
	 * Next 7 day segment in ISO 8601 format.
	 */
	nextStartDate: string;
	/**
	 * Previous 7 day segment in ISO 8601 format.
	 */
	previousStartDate: string;
	/**
	 * Games occuring during next 7 days.
	 */
	gameWeek: {
		date: string;
		dayAbbrev: "SUN" | "MON" | "TUE" | "WED" | "THU" | "FRI" | "SAT";
		numberOfGames: number;
		games: Game[];
	}[];
	/**
	 * List of NHL Partners offering odds on the games.
	 */
	oddsPartners: OddsPartner[];
	/**
	 * Start date of the pre-season in ISO 8601 format.
	 */
	preSeasonStartDate: string;
	/**
	 * Start date of the regular season in ISO 8601 format.
	 */
	regularSeasonStartDate: string;
	/**
	 * End date of the regular season in ISO 8601 format.
	 */
	regularSeasonEndDate: string;
	/**
	 * End date of the playoffs in ISO 8601 format.
	 */
	playoffEndDate: string;
	/**
	 * Number of games in 7 day segment.
	 */
	numberOfGames: number;
}

export interface TeamScheduleWeek {
	/**
	 * Previous 7 day segment in ISO 8601 format.
	 */
	previousStartDate: string;
	/**
	 * Next 7 day segment in ISO 8601 format.
	 */
	nextStartDate: string;
	calendarUrl: string;
	clubTimezone: string;
	clubUTFOffset: string;
	games: Game[];
}

export interface TeamScheduleMonth {
	previousMonth: string;
	currentMonth: string;
	nextMonth: string;
	calendarUrl: string;
	clubTimezone: string;
	clubUTCOffset: string;
	games: Game[];
}

export interface TeamScheduleSeason {
	previousSeason: number;
	currentSeason: number;
	clubTimezone: string;
	clubUTFOffset: string;
	games: Game[];
}

/// TODO: Determine rest
export enum GameType {
	//Preseason = 0,
	Regular = 1,
	//Playoffs = 2,
	//AllStar = 3,
}

/// TODO: Determine rest
export enum GameState {
	Final = "FINAL",
	Future = "FUT",
}

/// TODO: Determine rest
export enum GameScheduleState {
	Ok = "OK",
}

export enum TvMarket {
	North = "N",
}

export interface TvBroadcast {
	id: number;
	market: TvMarket;
	countryCode: string;
	network: string;
	sequenceNumber: number;
}

/// TODO: Determine rest
export enum PeriodType {
	Shootout = "SO",
}

export interface PeriodDescriptor {
	periodType?: PeriodType;
}

export interface GameTeam {
	id: number;
	placeName: LocalizedName;
	abbrev: TeamAbbreviation;
	logo: string;
	darkLogo: string;
	awaySplitSquad?: boolean;
	homeSplitSquad?: boolean;
	radioLink?: string;
	airlineLink?: string;
	airlineDesc?: string;
	hotelLink?: string;
	hotelDesc?: string;
	score?: number;
}

export interface Game {
	id: number;
	season: number;
	gameType: GameType;
	gameDate: string;
	venue: LocalizedName;
	neutralSite: false;
	startTimeUTC: string;
	easternUTCOffset: string;
	venueUTCOffset: string;
	venueTimezone: string;
	gameState: GameState;
	tvBroadcasts: TvBroadcast[];
	awayTeam: GameTeam;
	homeTeam: GameTeam;
	periodDescriptor: PeriodDescriptor;
	gameOutcome: unknown;
}

/**
 * NHL Partners offering odds on the games.
 */
export interface OddsPartner {
	partnerId: number;
	/**
	 * Two letter country code.
	 */
	country: string;
	name: string;
	imageUrl: string;
	siteUrl: string;
	/**
	 * CSS hex color.
	 */
	bgColor: string;
	/**
	 * CSS hex color.
	 */
	textColor: string;
	/**
	 * CSS hex color.
	 */
	accentColor: string;
}

export interface Standings {
	wildCardIndicator: boolean;
	standings: TeamStandings[];
}

export interface TeamStandings {
	conferenceHomeSequence: number;
	conferenceL10Sequence: number;
	conferenceRoadSequence: number;
	conferenceSequence: number;
	date: string;
	divisionHomeSequence: number;
	divisionL10Sequence: number;
	divisionRoadSequence: number;
	divisionSequnce: number;
	gameTypeId: number; // This may be a GameType but unsure as it wouldn't make sense
	gamesPlayed: number;
	goalDifferential: number;
	goalDifferentialPctg: number;
	goalAgainst: number;
	goalFor: number;
	goalsForPctg: number;
	homeGamesPlayed: number;
	homeGoalDifferential: number;
	homeGoalsAgainst: number;
	homeGoalsFor: number;
	homeLosses: number;
	homePoints: number;
	homeRegulationPlusOtWins: number;
	homeRegulationWins: number;
	homeTimes: number;
	homeWins: number;
	l10GamesPlayed: number;
	l10GoalDifferential: number;
	l10GoalsAgainst: number;
	l10GoalsFor: number;
	l10Losses: number;
	l10OtLosses: number;
	l10Points: number;
	l10RegulationPlusOtWins: number;
	l10RegulationWins: number;
	l10Ties: number;
	l10Wins: number;
	leagueHomeSequence: number;
	leagueL10Sequence: number;
	leagueRoadSequence: number;
	leagueSequence: number;
	losses: number;
	otLosses: number;
	placeName: LocalizedName;
	pointPctg: number;
	points: number;
	regulationPlusOtWinPct: number;
	regulationPlusOtWins: number;
	regulationWinPctg: number;
	regulationWins: number;
	roadGamesPlayed: number;
	roadGoalDifferential: number;
	roadGoalsAgainst: number;
	roadGoalsFor: number;
	roadLosses: number;
	roadOtLosses: number;
	roadPoints: number;
	roadRegulationPlusOtWins: number;
	roadRegulationWins: number;
	roadTies: number;
	roadWins: number;
	seasonId: number;
	shootoutLosses: number;
	shootoutWins: number;
	streakCode: "W" | "L";
	streakCount: number;
	teamName: LocalizedName;
	teamCommonName: LocalizedName;
	teamAbbrev: {
		default: TeamAbbreviation;
	};
	teamLogo: string;
	ties: number;
	waiversSequence: number;
	wildcardSequence: number;
	winPctg: number;
	wins: number;
}

export interface StandingsFormat {
	/** ISO 8601 date string */
	currentDate: string;
	seasons: StandingsSeasonFormat[];
}

export interface StandingsSeasonFormat {
	/** Season ID */
	id: number;
	conferencesInUse: boolean;
	divisionsInUse: boolean;
	pointForOTlossInUse: boolean;
	regulationWinsInUse: boolean;
	rowInUse: boolean;
	/** ISO 8601 date string */
	standingsEnd: string;
	/** ISO 8601 date string */
	standingsStart: string;
	tiesInUse: boolean;
	wildcareInUse: boolean;
}

export interface TeamSeasonStats {
	season: number;
	gameTypes: GameType[];
}
