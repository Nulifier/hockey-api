import { expect, test } from "vitest";
import { formatDate, formatSeasonYear } from "./util.js";

test("it should format a date", () => {
	const d = new Date(2020, 1, 22);
	expect(formatDate(d)).toBe("2020-02-22");
});

test("it should format a date using only the month", () => {
	const d = new Date(2020, 1, 22);
	expect(formatDate(d, true)).toBe("2020-02");
});

test("it should format a season", () => {
	expect(formatSeasonYear(2020)).toBe("20202021");
});
