import fetch from "node-fetch";

export function request<T>(url: string) {
	return fetch(url, {
		method: "GET",
	}).then((res) =>
		(res.json() as Promise<T>).catch(async (err) => {
			console.error("Error parsing JSON:");
			console.error(await res.text());
			throw err;
		})
	);
}
