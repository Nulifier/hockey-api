import { describe, expect, test } from "vitest";
import * as hockey from "./hockey.js";
import { TeamAbbreviation } from "./teams.js";

describe("schedule", () => {
	test("returns current day of games for the league", async () => {
		const res = await hockey.getLeagueScheduleDay();
		expect(res.gameWeek).toBeDefined();
	});

	test("returns specific day of games for the league", async () => {
		const res = await hockey.getLeagueScheduleDay(new Date("2024-01-22"));
		expect(res.gameWeek).toBeDefined();
		expect(res.gameWeek.length).toBe(1);
		expect(res.numberOfGames).toBe(6);
	});

	test("returns current day of games for a team", async () => {
		const res = await hockey.getTeamScheduleDay(TeamAbbreviation.Oilers);
		expect(res.games).toBeDefined();
	});

	test("returns specific day of games for a team", async () => {
		const res = await hockey.getTeamScheduleDay(
			TeamAbbreviation.Oilers,
			new Date("2024-01-20")
		);
		expect(res.games).toBeDefined();
		expect(res.games.length).toBe(1);
	});

	test("returns current week of games for the league", async () => {
		const res = await hockey.getLeagueScheduleWeek();
		expect(res.gameWeek).toBeDefined();
	});

	test("returns specific week of games for the league", async () => {
		const res = await hockey.getLeagueScheduleWeek(new Date("2024-01-22"));
		expect(res.gameWeek).toBeDefined();
		expect(res.numberOfGames).toBe(51);
	});

	test("returns current week of games for a team", async () => {
		const res = await hockey.getTeamScheduleWeek(TeamAbbreviation.Oilers);
		expect(res.games).toBeDefined();
	});

	test("returns specific week of games for a team", async () => {
		const res = await hockey.getTeamScheduleWeek(
			TeamAbbreviation.Oilers,
			new Date("2024-01-22")
		);
		expect(res.games).toBeDefined();
		expect(res.games.length).toBe(3);
	});

	test("returns current month of games for a team", async () => {
		const res = await hockey.getTeamScheduleMonth(TeamAbbreviation.Oilers);
		expect(res.games).toBeDefined();
	});

	test("returns specific week of games for a team", async () => {
		const res = await hockey.getTeamScheduleMonth(
			TeamAbbreviation.Oilers,
			new Date("2024-01-22")
		);
		expect(res.games).toBeDefined();
		expect(res.games.length).toBe(11);
	});

	test("returns all games in a season for a team", async () => {
		const res = await hockey.getTeamScheduleSeason(TeamAbbreviation.Oilers);
		expect(res.games).toBeDefined();
	});

	test("checks if team playing today", async () => {
		const res = await hockey.isTeamPlaying(TeamAbbreviation.Oilers);
		expect(res).toBeTypeOf("boolean");
	});

	test("checks if team playing on a specific date", async () => {
		const res1 = await hockey.isTeamPlaying(
			TeamAbbreviation.Oilers,
			new Date("2024-01-20")
		);
		expect(res1).toBe(true);
		const res2 = await hockey.isTeamPlaying(
			TeamAbbreviation.Oilers,
			new Date("2024-01-21")
		);
		expect(res2).toBe(false);
	});
});

describe("standings", () => {
	test("Get standings for this season", async () => {
		const res = await hockey.getStandings();
		expect(res.standings.length).toBeGreaterThan(1);
	});

	test("Get standings for a season", async () => {
		const res = await hockey.getStandings(new Date("1988-01-01"));
		expect(res.standings.length).toBeGreaterThan(1);
		const oilers = res.standings.filter(
			(s) => s.placeName.default === "Edmonton"
		);
		expect(oilers[0].wins === 27);
	});

	test("Get information about standings for a season", async () => {
		const res = await hockey.getStandingsFormat();
		expect(res.seasons.length).toBeGreaterThan(10);
		const firstSeason = res.seasons[0];
		expect(firstSeason.id).toBe(19171918);
	});
});

describe("stats", () => {
	test("Get team stats by season", async () => {
		const res = await hockey.getTeamSeasonStats(TeamAbbreviation.Oilers);
		expect(res.length).toBeGreaterThan(10);
	});
});
