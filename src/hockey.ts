import type {
	TeamScheduleMonth,
	TeamScheduleWeek,
	LeagueScheduleWeek,
	TeamScheduleSeason,
	Standings,
	StandingsFormat,
	TeamSeasonStats,
} from "./hockey-types.js";
import { request } from "./request.js";
import { TeamAbbreviation } from "./teams.js";
import { formatDate } from "./util.js";

// This information is based off the excellent research done here:
// https://gitlab.com/dword4/nhlapi/-/blob/master/new-api.md

const WEB_ROOT = "https://api-web.nhle.com/v1";

///////////////////////////////////////
// Schedule

export async function getLeagueScheduleDay(
	date = new Date()
): Promise<LeagueScheduleWeek> {
	const dateStr = formatDate(date);
	const weekSchedule = await getLeagueScheduleWeek(date);
	const daySchedule = {
		...weekSchedule,
		gameWeek: weekSchedule.gameWeek.filter((day) => day.date === dateStr),
	};
	// There should only be one day left
	if (daySchedule.gameWeek.length === 1) {
		daySchedule.numberOfGames = daySchedule.gameWeek[0].numberOfGames;
	}
	return daySchedule;
}

export async function getTeamScheduleDay(
	team: TeamAbbreviation,
	date = new Date()
): Promise<TeamScheduleWeek> {
	const dateStr = formatDate(date);
	const weekSchedule = await getTeamScheduleWeek(team, date);
	return {
		...weekSchedule,
		games: weekSchedule.games.filter((game) => game.gameDate === dateStr),
	};
}

export function getLeagueScheduleWeek(date?: Date) {
	const URL = `${WEB_ROOT}/schedule/${date ? formatDate(date) : "now"}`;
	return request<LeagueScheduleWeek>(URL);
}

export function getTeamScheduleWeek(team: TeamAbbreviation, date?: Date) {
	const URL = `${WEB_ROOT}/club-schedule/${team}/week/${
		date ? formatDate(date) : "now"
	}`;
	return request<TeamScheduleWeek>(URL);
}

export function getTeamScheduleMonth(team: TeamAbbreviation, date?: Date) {
	const URL = `${WEB_ROOT}/club-schedule/${team}/month/${
		date ? formatDate(date, true) : "now"
	}`;
	return request<TeamScheduleMonth>(URL);
}

export function getTeamScheduleSeason(team: TeamAbbreviation) {
	const URL = `${WEB_ROOT}/club-schedule-season/${team}/now`;
	return request<TeamScheduleSeason>(URL);
}

export async function isTeamPlaying(
	team: TeamAbbreviation,
	date?: Date
): Promise<boolean> {
	const schedule = await getTeamScheduleDay(team, date);
	return schedule.games.length > 0;
}

///////////////////////////////////////
// Standings

export function getStandings(date?: Date) {
	const URL = `${WEB_ROOT}/standings/${date ? formatDate(date) : "now"}`;
	return request<Standings>(URL);
}

export function getStandingsFormat() {
	return request<StandingsFormat>(`${WEB_ROOT}/standings-season`);
}

///////////////////////////////////////
// Stats

export function getTeamSeasonStats(team: TeamAbbreviation) {
	return request<TeamSeasonStats[]>(`${WEB_ROOT}/club-stats-season/${team}`);
}

// export function getPlayerStats(playerId: number) {
// 	return request<unknown>(`${WEB_ROOT}/player/${playerId}/landing`);
// }

///////////////////////////////////////
// Rosters

///////////////////////////////////////
// Game Details
