import { expect, test } from "vitest";
import { request } from "./request.js";

test("it should fail on an invalid address", async () => {
	expect.hasAssertions();
	return expect(
		request("https://api.nhle.com/stats/invalid")
	).rejects.toThrow();
});

test("it should fail on a non-JSON payload", async () => {
	expect.hasAssertions();
	return expect(request("https://example.com")).rejects.toThrow();
});

test("it should succeed on a valid GET request", async () => {
	return request("https://api.nhle.com/stats/rest/en/franchise");
});

test("it should parse the JSON response", async () => {
	const res = await request<any>(
		"https://api.nhle.com/stats/rest/en/franchise"
	);
	expect(res.total).toBeGreaterThan(0);
});
