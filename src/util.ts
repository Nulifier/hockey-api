/**
 * Formats a date in the format that the API expects it (YYYY-MM-DD)
 */
export function formatDate(date: Date, monthOnly = false) {
	if (monthOnly) {
		return `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(
			2,
			"0"
		)}`;
	} else {
		return `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(
			2,
			"0"
		)}-${String(date.getDate()).padStart(2, "0")}`;
	}
}

/**
 * Formats a year into a season.
 * @param year The starting year of the season
 */
export function formatSeasonYear(year: number) {
	return `${year}${year + 1}`;
}

export type PickPartial<T, K extends keyof T> = Partial<Pick<T, K>> &
	Omit<T, K>;

export type PickRequired<T, K extends keyof T> = Required<Pick<T, K>> &
	Omit<T, K>;
